class AddCreatedAtIndexToTweets < ActiveRecord::Migration[5.0]
  def change
    # We are doing this because there is a real lag in performance
    # from "keywords#show" controller where we are ordering tweets.
    add_index :tweets, :created_at
  end
end
