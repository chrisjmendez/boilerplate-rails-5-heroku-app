class CreateTweets < ActiveRecord::Migration[5.0]
  def change
    create_table :tweets do |t|
      t.string :tweet_id
      t.string :tweet_created_at
      t.text :text
      t.string :user_uid
      t.string :user_name
      t.string :user_screen_name
      t.string :user_image_url
      t.belongs_to :keyword, foreign_key: true

      t.timestamps
    end

  end
end
