class AddImageToKeywords < ActiveRecord::Migration[5.0]
  def change
    add_column :keywords, :image, :string
  end
end
