# Unicorn Configuration
# Purpose: Reduces the startup time of individual Unicorn 
# https://devcenter.heroku.com/articles/rails-unicorn
worker_processes Integer(ENV["WEB_CONCURRENCY"] || 3)

timeout 15

# Used for more accurate data collection within unicorn apps
preload_app true

# manage the external connections of each individual worker
before_fork do |server, worker|
  Signal.trap 'TERM' do
    puts 'Unicorn master intercepting TERM and sending myself QUIT instead'
    Process.kill 'QUIT', Process.pid
  end

  defined?(ActiveRecord::Base) and
  ActiveRecord::Base.connection.disconnect!
end

after_fork do |server, worker|
  Signal.trap 'TERM' do
    puts 'Unicorn worker intercepting TERM and doing nothing. Wait for master to send QUIT'
  end

  defined?(ActiveRecord::Base) and
  ActiveRecord::Base.establish_connection
end

