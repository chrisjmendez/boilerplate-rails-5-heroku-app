namespace :heroku do
  desc %Q{ ›› Heroku - Restart server }
  task restart: :environment do
    sh %{ heroku restart }
  end

  desc %Q{ ›› Heroku - Realtime Logs }
  task realtime: :environment do
    sh %{ heroku logs -t }
  end

  desc %Q{ ›› Heroku - Print Heroku logs to ~/Desktop }
  task logs: :environment do
    num_of_lines = 1000
    local_path   = "~/Desktop/heroku-logs.log"
    sh %{ heroku logs -n #{num_of_lines} > #{local_path} }
  end

  desc %Q{ ›› Heroku - Print Heroku logs and filter by keyword }
  task logs: :environment do
    keyword = "my keyword"
    sh %{ heroku logs -t | grep #{keyword} }
  end

  desc %Q{ ›› Heroku - Check status of DB }
  task db: :environment do
    sh %{ heroku pg:info }
  end

  desc %Q{ ›› Heroku - Review Config variables }
  task config: :environment do
    sh %{ heroku config }
  end

  desc %Q{ ›› Heroku - Backup }
  task backup: :environment do
    sh %{ open http://www.chrisjmendez.com/2016/12/19/upgrading-your-database-on-heroku/ }    
  end

  desc %Q{ ›› Heroku - Check server status }
  task status: :environment do
    sh %{ heroku ps:scale web=1 }
  end

  desc %Q{ ›› Heroku - Set an ENVIRONMENTAL VARIABLE }
  # http://www.chrisjmendez.com/2016/12/14/publish-your-new-rails-5-app-to-heroku/
  task store_var: :environment do
    key   = VAR_IN_UPPERCASE
    value = value
    sh %{ heroku config:set #{key}=#{value} }
  end

end
