namespace :scheduler do
  desc %Q{ ›› Update Feed }
  task update_feed: :environment do
    Keyword.collect_all_tweets
  end

end
