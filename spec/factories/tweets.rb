FactoryGirl.define do
  factory :tweet do
    tweet_id "MyString"
    tweet_created_at "MyString"
    text "MyText"
    user_uid "MyString"
    user_name "MyString"
    user_screen_name "MyString"
    user_image_url "MyString"
    keyword nil
  end
end
