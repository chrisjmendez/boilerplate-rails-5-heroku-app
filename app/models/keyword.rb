class Keyword < ApplicationRecord  
  NUMBER_OF_TWEETS = 50

  has_many :tweets
  
  validates :word, presence: true

  # Carrier Wave
  mount_uploader :image, ImageUploader
  
  def collect_tweets
    client = Twitter::REST::Client.new do |config|
      config.consumer_key        = ENV['TWITTER_CONSUMER_KEY']
      config.consumer_secret     = ENV['TWITTER_CONSUMER_SECRET']
      config.access_token        = ENV['TWITTER_ACCESS_TOKEN']
      config.access_token_secret = ENV['TWITTER_ACCESS_SECRET']
    end  
    
    # Create a collection of Tweets that match the search keyword
    client.search(self.word, count: NUMBER_OF_TWEETS, result_type: "recent", lang: "en").take(NUMBER_OF_TWEETS).collect do |t|
      tweet = Tweet.new if t.is_a?(Twitter::Tweet)
      tweet.tweet_id         = t.id.to_s
      tweet.tweet_created_at = tweet.created_at
      tweet.text             = t.text
      tweet.user_uid         = t.user.id
      tweet.user_name        = t.user.name
      tweet.user_screen_name = t.user.screen_name
      tweet.user_image_url   = t.user.profile_image_url
      tweet.keyword          = self
      
      tweet.save
    end
  end
  
  # Grant a specific number of Tweets for each keyword
  def self.collect_all_tweets
    Keyword.all.each do |keyword|
      keyword.collect_tweets
    end
    # Memcachier that clears a fragment
    ActionController::Base.new.expire_fragment('keywords_index_table')
  end
end
