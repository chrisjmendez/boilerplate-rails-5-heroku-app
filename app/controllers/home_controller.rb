class HomeController < ApplicationController
  def index
    users = User.all.limit(2)
    @admin = users.first
    @user  = users.second
  end
  
  def loaderio
    if Rails.env.production?
      # http://blog.teamtreehouse.com/static-pages-ruby-rails
      # http://guides.rubyonrails.org/layouts_and_rendering.html
      render text: "#{ENV['LOADER_IO']}", status: :ok, layout: false
    else
      render file: "public/404.html", status: :not_found
    end
  end
end
