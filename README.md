# Twitter Search

The app itself is arbitrary. The real purpose of this application is to show you how to create a production-ready Rails 5 app for Heroku.

This app demonstrates how to pull data from Twitter every 10 minutes based on a keyword. 

## Getting Started

You'll need to do a few things to get up-and-running.  Here are a few items:

* Install all the necessary gems with ```bundle install```
* Modify ```config/database.yml``` with a new database name.
* Update your database with ```rails db:wipe```
* Start your web server with ```rails server:start```
* Create an account with [http://mailgun.com](http://mailgun.com) and update your SMPT data in ```config/environments/development.rb```.
* Change your ```ENV['SECRET_KEY_BASE']``` [Source](https://matt.aimonetti.net/posts/2013/11/30/sharing-rails-sessions-with-non-ruby-apps/)


---

# What does this app do?

Read this [tutorial](http://www.chrisjmendez.com/2016/12/13/publish-tweet-data-using-rails-console-to-json-or-yaml/) to get a basic understanding of what this app offers. This app takes the Rails Console example and turns it into a Ruby on Rails application.

This app also offers a few neat features including:

* It's Heroku-ready. Read this [tutorial](http://chrisjmendez.com/publish-your-new-rails-5-app-to-heroku) to learn more. 
* It uses Heroku's [cron job scheduler](https://devcenter.heroku.com/articles/scheduler) to collect tweets every 10 minutes through ```lib/tasks/scheduler.rake```.
* This app stores the Tweets straight to Postgresql.
* This app allows you to create new Twitter search keywords and it will auto-populate the entry with new tweets.

---

# Configuring Heroku

- [Postgres Database](https://elements.heroku.com/addons/heroku-postgresql) using a Heroku Add-On
- [New Relic APM](https://elements.heroku.com/addons/newrelic) for monitoring server performance
- [Unicorn web server](https://devcenter.heroku.com/articles/rails-unicorn) to handle multiple, concurrent processes using a single dyno.
- [Papertrain](https://elements.heroku.com/addons/papertrail) for better logging.
- [Heroku Scheduler](https://elements.heroku.com/addons/scheduler) for cron job automation.
- [Autobus[(https://elements.heroku.com/addons/autobus)] for automatic database backups.
- [Carrierwave](https://github.com/carrierwaveuploader/carrierwave) for file uploading.
- [Fog](https://github.com/fog/fog) for AWS file upload integration


---


# Resources

* [Twitter Gem](http://sferik.github.io/twitter/) - This is the gem we use to pull data from Twitter and store it into a database. 
* [Scheduling Cron Jobs with Heroku](https://devcenter.heroku.com/articles/scheduler)
* [Heroku Scheduling Dashboard](https://scheduler.heroku.com/dashboard)
* [How to configure Devise and OmniAuth](https://www.digitalocean.com/community/tutorials/how-to-configure-devise-and-omniauth-for-your-rails-application)
* [Javascript to Coffee](http://js2.coffee/)
* [HTML to HAML](http://htmltohaml.com/)
* [Devise AJAX Authentication](http://blog.andrewray.me/how-to-set-up-devise-ajax-authentication-with-rails-4-0/)
* [Asset Pipeline Simple](http://blog.commandrun.com/rails-asset-pipeline-simple-guide/)
